package com.sda.threading.cards;

public class Card {
    private CardColor color;
    private CardType type;

    public Card(CardColor color, CardType type) {
        this.color = color;
        this.type = type;
    }

    public CardColor getColor() {
        return color;
    }

    public void setColor(CardColor color) {
        this.color = color;
    }

    public CardType getType() {
        return type;
    }

    public void setType(CardType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Card{" +
                "color=" + color +
                ", type=" + type +
                '}';
    }
}
