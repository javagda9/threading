package com.sda.threading.cards;

import java.util.ArrayList;
import java.util.List;

public class Talia {
    private List<Card> cards = new ArrayList<>();

    public void generate(){
        for (CardType type: CardType.values()) {
            for (CardColor color: CardColor.values()) {
                Card c = new Card(color, type);
                cards.add(c);
            }
        }
    }

    public void print(){
        for (Card c : cards) {
            System.out.println(c);
        }
    }
}
