package com.sda.threading.cards;

public enum CardType {
    C2(2), C3(3), C4(4), C5(5), C6(6), C7(7), C8(8), C9(9), C10(10),
    J(11), Q(12), K(13), A(14);

    private int cardValue;

    CardType(int val) {
        cardValue = val;
    }

    public int getCardValue() {
        return cardValue;
    }

    public void setCardValue(int cardValue) {
        this.cardValue = cardValue;
    }

    @Override
    public String toString() {
        return super.toString() + "-" + cardValue;
    }
}
