package com.sda.threading.workers;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Workplace implements IWorkplace {
    private ExecutorService pulaWatkow = Executors.newFixedThreadPool(6);

    private int iloscDlugopisow = 0;
    private int iloscObudow = 0;
    private int iloscWkladow = 0;

    public Workplace() {
        // 5 pracownikow
        Worker w1 = new Worker("Marian", this, WorkerType.CASE);
        Worker w2 = new Worker("Michau", this, WorkerType.INSERT);
        Worker w3 = new Worker("Rafał", this, WorkerType.CASE);
        Worker w4 = new Worker("Paweu", this, WorkerType.INSERT);
        // wysyłam do puli
        pulaWatkow.submit(w1);
        pulaWatkow.submit(w2);
        pulaWatkow.submit(w3);
        pulaWatkow.submit(w4);

        Worker w5 = new Worker("Gaweu", this, WorkerType.PEN);
        Worker w6 = new Worker("Gaweu", this, WorkerType.PEN);
        pulaWatkow.submit(w5);
        pulaWatkow.submit(w6);
    }

    public void addWorker(String name) {
        Worker w = new Worker(name, this, WorkerType.PEN);
        pulaWatkow.submit(w);
    }

    public int getIloscDlugopisow() {
        return iloscDlugopisow;
    }

    public void setIloscDlugopisow(int iloscDlugopisow) {
        this.iloscDlugopisow = iloscDlugopisow;
    }

    public void stopWork() {
        pulaWatkow.shutdown();
    }

    @Override
    public boolean czyJestWkladIObudowa() {
        return iloscWkladow > 0 && iloscObudow > 0;
    }

    @Override
    public void pobierzWkladIObudowe() {
        iloscWkladow--;
        iloscObudow--;
    }

    @Override
    public void dodajDlugopis() {
        iloscDlugopisow++;
    }

    @Override
    public void dodajWklad() {
        iloscWkladow++;
    }

    @Override
    public void dodajObudowe() {
        iloscObudow++;
    }
}
