package com.sda.threading.workers;

public interface IWorkplace {
    void dodajDlugopis();
    void dodajWklad();
    void dodajObudowe();
    boolean czyJestWkladIObudowa();
    void pobierzWkladIObudowe();
}
