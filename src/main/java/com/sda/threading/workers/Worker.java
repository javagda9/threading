package com.sda.threading.workers;

import java.util.Random;

public class Worker implements Runnable {
    private static final int SLEEP_WKLAD = 3000;
    private static final int SLEEP_OBUDOWA = 2000;
    private static final int SLEEP_SKLADANIE = 2000;
    private String name;
    private boolean isWorking;

    private WorkerType type;

    private IWorkplace miejscePracy;

    public Worker(String name, IWorkplace miejscePracy, WorkerType type) {
        this.name = name;
        this.miejscePracy = miejscePracy;
        this.type = type;
    }

    @Override
    public void run() {
        Random random = new Random();
        System.out.println("Zaczynam");
        isWorking = true;

        while (isWorking) {
            try {
                if (type == WorkerType.INSERT) {
                    Thread.sleep(SLEEP_WKLAD - random.nextInt(1000));
                    System.out.println("Skończyłem wkład");

                    miejscePracy.dodajWklad();
                } else if (type == WorkerType.CASE) {
                    Thread.sleep(SLEEP_OBUDOWA + random.nextInt(1000));
                    System.out.println("Skończyłem obudowę");

                    miejscePracy.dodajObudowe();
                } else if (type == WorkerType.PEN) {
                    // sprawdzenie ilosci wkladow i obudow
                    // pobranie po 1 wkladzie i obudowie
                    // praca
                    // zwrocenie dlugopisu
                    if(miejscePracy.czyJestWkladIObudowa()) {
                        miejscePracy.pobierzWkladIObudowe();

                        Thread.sleep(SLEEP_SKLADANIE - random.nextInt(1000));
                        System.out.println("Skompletowałem długopis");
                        miejscePracy.dodajDlugopis();
                    }else{
                        System.out.println("Brak dlugopisow");
                        Thread.sleep(1000); // faja
                    }
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
