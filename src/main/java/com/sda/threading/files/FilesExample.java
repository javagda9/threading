package com.sda.threading.files;

import java.io.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Scanner;

public class FilesExample {
    public static void main(String[] args) {
        // stworzenie obiektu File
        File plik = new File("output_0.txt");
        Scanner sc = new Scanner(System.in);

        // stworzenie "Writera" który stworzy plik i bedzie do niego dopisywać (linia nizej)
        //try(PrintWriter writer = new PrintWriter(new FileOutputStream(plik, true))){

        // stworzenie pliku i nadpisanie starych wartości (linia nizej)

        Timestamp t = new Timestamp(plik.lastModified());
        LocalDateTime ldt = LocalDateTime.ofInstant(t.toInstant(), ZoneId.of("Europe/Warsaw"));

        try (PrintWriter writer = new PrintWriter(plik)) {
//            while (true) {
                String line = sc.nextLine();
                writer.println(line);
                writer.flush();
//            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


    }
}
