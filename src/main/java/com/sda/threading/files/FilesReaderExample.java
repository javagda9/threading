package com.sda.threading.files;

import java.io.*;
import java.util.Scanner;

public class FilesReaderExample {
    public static void main(String[] args) {

        File plik = new File("output_0.txt");

        // try with resources
        try (Scanner sc = new Scanner(plik)){
            while(sc.hasNextLine()){
                String linia = sc.nextLine();
                System.out.println(linia);
            }
            System.out.println("Koniec pliku");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(plik))){
            String linia = bufferedReader.readLine();
            while (linia != null){
                System.out.println(linia);
                linia = bufferedReader.readLine();
            }
            System.out.println("Koniec pliku");
        } catch (IOException e) {
            e.printStackTrace();
        }


//        Scanner sc = new Scanner(System.in);
//
//        String line = sc.nextLine();
//        System.out.println(line);
//        System.out.println();
//        System.out.println("Faza");
//
//        line = sc.next();
//        System.out.println(line);
//        System.out.println();
//        System.out.println("Faza");
//
//        line = sc.nextLine();
//        System.out.println(line);
//        System.out.println();
//        System.out.println("Faza");
//
//        int val = sc.nextInt();
//        System.out.println(val);
//        System.out.println();
//        System.out.println("Faza");
//
//        line = sc.nextLine();
//        System.out.println(line);
//        System.out.println();


    }
}
