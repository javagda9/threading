package com.sda.threading.bank;

public class ZleceniePrzelewu implements Runnable {
    private BankAccount konto;
    private double howMuch;
    private KierunekPrzelewu kierunek;

    public ZleceniePrzelewu(BankAccount konto, double howMuch, KierunekPrzelewu kierunek) {
        this.konto = konto;
        this.howMuch = howMuch;
        this.kierunek = kierunek;
    }

    @Override
    public void run() {
        if (kierunek == KierunekPrzelewu.SUB) {
            konto.sub(howMuch);
        } else if (kierunek == KierunekPrzelewu.ADD) {
            konto.add(howMuch);
        }
    }
}
