package com.sda.threading;

import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadingMain {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

//        Runnable obiekt = new Runnable() {
//            public void run() {
//
//                for (int i = 0; i < 10; i++) {
//                    System.out.println("Hello thread!");
//                }
//            }
//        };
        ExecutorService pula = Executors.newCachedThreadPool();


        boolean isWorking = true;
        while (isWorking) {

            String linia = sc.next();
            if (linia.equals("quit")) {
                break;
            }
            int ileCzasu = Integer.parseInt(linia);

            ThreadTask zadanie = new ThreadTask(ileCzasu);

//            pula.submit(zadanie);
            Thread watek = new Thread(zadanie);
            watek.start();
        }

    }
}
