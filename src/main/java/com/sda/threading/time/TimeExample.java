package com.sda.threading.time;

import java.sql.Timestamp;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TimeExample {
    public static void main(String[] args) {
//        ExecutorService executorService = Executors.newFixedThreadPool(5);
        LocalDateTime dt = LocalDateTime.now();
//        LocalDateTime dt2 = LocalDateTime.of();
        System.out.println(dt);

        long milisSince1970 = System.currentTimeMillis();
        Timestamp timestamp = Timestamp.valueOf(LocalDateTime.now());

        LocalDate ld = LocalDate.now();
        System.out.println(ld);

        LocalTime lt = LocalTime.now();
        System.out.println(lt);

//        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm (ss)");
        String sformatowane = dtf.format(dt);

        System.out.println(sformatowane);

        // do zadania
//        String linia = null;// scanner.next();
//        LocalDateTime data1 = LocalDateTime.parse(linia, dtf);

//        Period p = Period.between(data1, data2);

//        Duration d = Duration.between(czas1, czas2);

        long milisSince1970Stop = System.currentTimeMillis();
        Timestamp timestampStop = Timestamp.valueOf(LocalDateTime.now());
        System.out.println(timestampStop.getTime() - timestamp.getTime());
        System.out.println(milisSince1970Stop - milisSince1970);

        Duration d = Duration.between(timestamp.toInstant(),timestampStop.toInstant());

        System.out.println(d.toMillis());
    }
}
