package com.sda.threading.students;

public class Student {
    private int indeks;
    private String name;

    public Student(int indeks, String name) {
        this.indeks = indeks;
        this.name = name;
    }

    public int getIndeks() {
        return indeks;
    }

    public void setIndeks(int indeks) {
        this.indeks = indeks;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
